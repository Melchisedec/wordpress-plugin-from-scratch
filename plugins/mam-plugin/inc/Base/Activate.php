<?php
/**
 * @package  mamPlugin
 */
namespace Inc\Base;

class Activate
{
	public static function activate() {
		flush_rewrite_rules();

		if ( get_option( 'mam_plugin' ) ) {
			return;
		}

		$default = array();

		update_option( 'mam_plugin', $default );
	}
}