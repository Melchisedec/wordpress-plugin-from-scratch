<?php
/**
* Trigger this file on plugin uninstll
* @package mamPlugin
*
*/

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	die;
}

// for standalone post types METHOD 1;

// Clear DB stored data
$books = get_posts( [ 'post_type' => 'book', 'numberposts' => -1 ] );


foreach( $books as $book ) {
	wp_delete_post( $book->ID, true )
}

// METHOD 2; if your CPT had custom boxes and other elements & you need to delete all then use below
// global $wpdb;
// $wpdb->query( "DELETE FROM wp_plugin_1_posts where post_type = 'book' " );
// $wpdb->query( "DELETE FROM wp_plugin_1_postmeta WHERE post_id NOT IN ( SELECT id FROM wp_plugin_1_posts )" );
// $wpdb->query( "DELETE FROM wp_plugin_1_term_relationships WHERE object_id NOT IN ( SELECT id FROM wp_plugin_1_posts )" );

